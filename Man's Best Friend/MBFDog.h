//
//  MBFDog.h
//  Man's Best Friend
//
//  Created by Ryan Van Fleet on 5/27/14.
//  Copyright (c) 2014 Van Fleet. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MBFDog : NSObject


@property (nonatomic) int age;
@property (nonatomic, strong) NSString *breed;
@property (strong, nonatomic) UIImage *image;
@property (strong, nonatomic) NSString *name;

// Methods
-(void)bark;
-(void)barkANumberOfTimes:(int)numberOfTimes;
-(void)changeBreedToWarewolf;
-(void)barkANumberOfTimes:(int)numberOfTimes loudly:(BOOL)isLoud;
-(int)ageInDogYearsFromAge:(int)regularAge;

@end
