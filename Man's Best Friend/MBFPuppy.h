//
//  MBFPuppy.h
//  Man's Best Friend
//
//  Created by Ryan Van Fleet on 6/3/14.
//  Copyright (c) 2014 Van Fleet. All rights reserved.
//

#import "MBFDog.h"

@interface MBFPuppy : MBFDog

-(void)givePuppyEyes;

@end
