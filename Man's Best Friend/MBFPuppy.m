//
//  MBFPuppy.m
//  Man's Best Friend
//
//  Created by Ryan Van Fleet on 6/3/14.
//  Copyright (c) 2014 Van Fleet. All rights reserved.
//

#import "MBFPuppy.h"

@implementation MBFPuppy

-(void)givePuppyEyes{
    NSLog(@":(");
}

-(void)bark{
    [super bark];
    NSLog(@"whimper whimper!");
    [self givePuppyEyes];
}

@end
