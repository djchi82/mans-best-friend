//
//  MBFDog.m
//  Man's Best Friend
//
//  Created by Ryan Van Fleet on 5/27/14.
//  Copyright (c) 2014 Van Fleet. All rights reserved.
//

#import "MBFDog.h"

@implementation MBFDog

-(void)bark
{
    NSLog(@"Woof Woof!");
}

-(void)barkANumberOfTimes:(int)numberOfTimes
{
    for (int bark = 1; bark <= numberOfTimes; bark++){
        [self bark];	
    }
}

-(void)changeBreedToWarewolf{
        self.breed = @"Werewolf";	
}

-(void)barkANumberOfTimes:(int)numberOfTimes loudly:(BOOL)isLoud{
    
    NSString *barkSound;
    if (!isLoud) {
        barkSound = @"yip yip";
    }else{
        barkSound = @"Ruff Ruff!";
    }
    for (int bark = 1; bark <= numberOfTimes; bark++){
        NSLog(@"%@", barkSound);
    }
}

-(int)ageInDogYearsFromAge:(int)regularAge{
    return regularAge * 7;
}

@end