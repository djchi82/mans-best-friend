//
//  main.m
//  Man's Best Friend
//
//  Created by Ryan Van Fleet on 5/27/14.
//  Copyright (c) 2014 Van Fleet. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MBFAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MBFAppDelegate class]));
    }
}
