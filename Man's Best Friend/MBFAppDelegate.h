//
//  MBFAppDelegate.h
//  Man's Best Friend
//
//  Created by Ryan Van Fleet on 5/27/14.
//  Copyright (c) 2014 Van Fleet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MBFAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
