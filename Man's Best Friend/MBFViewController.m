//
//  MBFViewController.m
//  Man's Best Friend
//
//  Created by Ryan Van Fleet on 5/27/14.
//  Copyright (c) 2014 Van Fleet. All rights reserved.
//

#import "MBFViewController.h"
#import "MBFDog.h"
#import "MBFPuppy.h"

@interface MBFViewController ()

@end

@implementation MBFViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Initalize each dog
    MBFDog *angus = [[MBFDog alloc] init];
    angus.name = @"Angus";
    angus.breed = @"Golden Retriever";
    angus.age = 1; // year
    angus.image = [UIImage imageNamed:@"DSC02881.JPG"];
    
    MBFDog *stBernard = [[MBFDog alloc] init];
    stBernard.name = @"Nana";
    stBernard.breed = @"St Bernard";
    stBernard.age = 1;
    stBernard.image = [UIImage imageNamed:@"St.Bernard.JPG"];
    
    MBFDog *borderCollie = [[MBFDog alloc]init];
    borderCollie.name = @"Lassie";
    borderCollie.breed = @"Border Collie";
    borderCollie.image = [UIImage imageNamed:@"BorderCollie.jpg"];
    
    MBFDog *jrt = [[MBFDog alloc]init];
    jrt.name = @"Wishbone";
    jrt.breed = @"Jack Russel Terrier";
    jrt.image = [UIImage imageNamed:@"JRT.jpg"];
    
    MBFDog *greyHound = [[MBFDog alloc]init];
    greyHound.name = @"Speedy";
    greyHound.breed = @"Italian Greyhound";
    greyHound.image = [UIImage imageNamed:@"ItalianGreyhound.jpg"];
    
    //Initialize the array
    self.myDogs = [[NSMutableArray alloc]init];
    [self.myDogs addObject:angus];
    [self.myDogs addObject:stBernard];
    [self.myDogs addObject:borderCollie];
    [self.myDogs addObject:jrt];
    [self.myDogs addObject:greyHound ];
    
//    [self printDogs:self.myDogs];
    
    self.myImageView.image = angus.image;
    self.myNameLabel.text = angus.name;
    self.breedLabel.text = angus.breed;
    self.lastDog = 0;
    
    MBFPuppy *puppy = [[MBFPuppy alloc]init];
    puppy.breed = @"Portugese Water Dog";
    puppy.name = @"Bo O";
    puppy.image = [UIImage imageNamed:@"Bo.jpg"];
//    [puppy givePuppyEyes];
    [puppy bark];
    [self.myDogs addObject:puppy];
    
    
//    [self.myDogs insertObject:[[MBFDog alloc]init] atIndex:1];
//    [self.myDogs removeObject:greyHound];
//    [self printDogs:self.myDogs];
    
    
    //    NSLog(@"My dog is named %@ and its' age is %i the breed is %@", myDog.name, myDog.age, myDog.breed	);
//    [myDog bark];
//    [myDog barkANumberOfTimes:3];
//    NSLog(@"%@", myDog.breed);
//    [myDog changeBreedToWarewolf];
//    NSLog(@"%@", myDog.breed);
//    myDog.breed = @"Golden Retriever";    
//    NSLog(@"%@", myDog.breed);
//    [myDog barkANumberOfTimes:3 loudly:NO];
//    int ageInDogYears = [myDog ageInDogYearsFromAge:1];
//    NSLog(@"%i", ageInDogYears);
//    NSLog(@"%i", [myDog ageInDogYearsFromAge:5]);
    
}

-(void)printDogs:(NSMutableArray*)dogs{
    
    for(int i = 0; i< [dogs count]; i ++){
        MBFDog *dog = [dogs objectAtIndex:i];
        NSLog(@"Name: %@; Breed: %@", dog.name, dog.breed);
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)newDogBarButtonItemPressed:(UIBarButtonItem *)sender {
    
    int randomInt = arc4random() % [self.myDogs count];
    if(self.lastDog == randomInt){
        [self newDogBarButtonItemPressed:sender];
        return;
    }
    MBFDog *randDog = [self.myDogs objectAtIndex:randomInt];
//    NSLog(@"%i", randomInt);
//    self.myImageView.image = randDog.image;
//    self.myNameLabel.text = randDog.name;
//    self.breedLabel.text = randDog.breed;
    [UIView transitionWithView:self.view duration:1 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        self.myImageView.image = randDog.image;
        self.myNameLabel.text = randDog.name;
        self.breedLabel.text = randDog.breed;
    } completion:^(BOOL finished) {
        
    }];
    sender.title = @"And another";
    self.lastDog = randomInt;
}

-(void)printHelloWorld{
    NSLog(@"Hello World");
}

@end
