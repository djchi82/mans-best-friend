//
//  MBFViewController.h
//  Man's Best Friend
//
//  Created by Ryan Van Fleet on 5/27/14.
//  Copyright (c) 2014 Van Fleet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MBFViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *myImageView;
@property (strong, nonatomic) IBOutlet UILabel *myNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *breedLabel;
@property (strong, nonatomic) NSMutableArray *myDogs;
@property (nonatomic) int lastDog;

- (IBAction)newDogBarButtonItemPressed:(UIBarButtonItem *)sender;

-(void)printHelloWorld;

@end
